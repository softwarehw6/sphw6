class Game extends Thread
{  
  Grid grid, tempGrid;
  boolean editable;
  int refreshRate;
  PFont font;
  int days;

  Game(float size)
  {
    grid = new Grid (size);
    editable = true;
    refreshRate = 500;
    font = loadFont("font.vlw");
    textFont(font, 12);
    days = 0;
  }
  
  void draw(){
  }
  
  public void run(){
  background(0);

  if (grid != null) grid.draw();

  if (editable == false) {
    tempGrid = grid;
    grid = new Grid (width);
    tempGrid.count();

    for (Cell c : tempGrid.liveCells) {
      c.judgeDestiny();
      if (c.alive) {
        c.setCount(0);
        grid.setCellAt(c, c.rc);
        grid.liveCells.add(c);
      }
    }
    for (Cell nc : tempGrid.nearbyCells) {
      nc.judgeDestiny();
      if (nc.alive) {
        nc.setCount(0);
        grid.setCellAt(nc, nc.rc);
        grid.liveCells.add(nc);
      }
    }
  }
      try{Thread.sleep(refreshRate);}   
      catch(Exception e){println("something went wrong");}
      
  }
  
  //getters
  boolean isEditable()
  {return editable;}
  Grid getGrid()
  {return grid;}
  
  //setters
  void setRefresh(int r)
  {refreshRate = r;}
  
  void editOrPlay(){
    if (editable == true) {editable=false;}
    else {editable = true;}
  }
  
  void reset(){
  days =0; 
  editable = false;
  grid.clearGrid();
  }

  void addLiveCells(float x, float y)  // here, the repetitive cells should be removed
  {
    RowCol rc = grid.coordToRowCol((int)x,(int)y);
    grid.liveCells.add(new Cell(rc,true));
  }
}