public class Cell {

  Grid grid;
  boolean alive;
  RowCol rc;
  int count, dayOld;
  color liveCol;


  public Cell(RowCol rc, boolean alive) {
    this.rc = rc;
    setAlive(alive);
    setCount(0);
    dayOld = 0;
    liveCol =color(0, 230, 0);
  }


  public int getRow() {
    return rc.getRow();
  }

  public int getCol() {
    return rc.getCol();
  }


  public void setCount(int count) {
    this.count = count;
  }

  public void setAlive(boolean alive) {
    this.alive = alive;
  }
  
  
  protected void judgeDestiny() {
    if (alive && count < 2) setAlive(false);
    if (alive && count == 2 || count == 3) setAlive(true);
    if (alive && count > 3) setAlive(false);
    if (!alive && count == 3) setAlive(true);
  }

  void addCount () {
    count ++;
  }

  void minusCount () {
    count --;
  }

  void addCol() {
    if (dayOld > 8) return; 
    liveCol += color(30, 0, 30);
  }
}