import java.util.*;

import android.view.MotionEvent;
import ketai.sensors.*;
import ketai.ui.*;

KetaiSensor sensor;
KetaiGesture gesture;

Game game;
float d;
int a;

void setup()
{
  fullScreen();
  //size(800,800);
  
  sensor = new KetaiSensor(this);
  gesture = new KetaiGesture(this);
  
  game = new Game(width);
  d = 0;
}

void draw()
{
  background(0);
  
  if(!game.isEditable())  game.run();
  game.getGrid().draw();
  
}

//pinch for increase decrease
void onPinch(float x, float y, float r)//x,y of center, r is the distance change
{
  if(game.isEditable() == true && (r>10 || r<-10))
  {
    d+=r;
    game = new Game(width + d);
  }
  else return;
}

//add cells
void mouseDragged(){
  if(game.isEditable() == true){
    game.addLiveCells();
    return;
  }
    else return; 
}

//shake for clear
void onAccelerometerEvent(float x, float y, float z) //x,y,z in m/s2
{
  if (abs(x)>0.1 || abs(y)>0.1 || abs(z)>0.1)
  {
    game.reset();//
  }
}

public boolean surfaceTouchEvent(MotionEvent event) 
{
  //5finger change state
  a= event.getPointerCount();
  if (a==5 && event.getActionMasked() == MotionEvent.ACTION_POINTER_UP)
  {
    game.editOrPlay();
  }

  super.surfaceTouchEvent(event);
  return gesture.surfaceTouchEvent(event);
}



void keyPressed(){
 if (key =='e'){
  game.editOrPlay();
 }
}