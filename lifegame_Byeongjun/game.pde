class Game extends Thread
{  
  Grid grid, tempGrid;
  boolean editable;
  int refreshRate;
  PFont font;
  int days;
  int gSize;

  Game(float size)
  {
    grid = new Grid (size);
    editable = true;
    refreshRate = 500;
    font = createFont("font.vlw", 50);
    textFont(font);
    days = 0;
  }

  public void run() {

    tempGrid = grid;
    grid = new Grid (width + d);
    tempGrid.count();


    for (Cell c : tempGrid.liveCells) {
      c.judgeDestiny();
      if (c.alive) {
        c.setCount(0);
        grid.setCellAt(c, c.rc);
        grid.liveCells.add(c);
      }
    }
    for (Cell nc : tempGrid.nearbyCells) {
      nc.judgeDestiny();
      if (nc.alive) {
        nc.setCount(0);
        grid.setCellAt(nc, nc.rc);
        grid.liveCells.add(nc);
      }
    }

    println(grid.liveCells.size());

    for (Cell c1 : tempGrid.liveCells) {
      for (Cell c2 : grid.liveCells) {
        if (c1.getRow() == c2.getRow() && c1.getCol() == c2.getCol()) {
          c2.dayOld ++;
        }
      }
    }

    days++;
    fill(255);
    text("Days:  " + days, 20, height-30);
    text("Reproducing...", 20, height -100);

    try {
      Thread.sleep(refreshRate);
    }   
    catch(Exception e) {
      println("something went wrong");
    }
  }

  //getters
  boolean isEditable()
  {
    return editable;
  }
  Grid getGrid()
  {
    return grid;
  }

  //setters
  void setRefresh(int r)
  {
    refreshRate = r;
  }

  void editOrPlay() {
    if (editable == true) {
      editable=false;
    } else {
      editable = true;
    }
  }

  void reset() {
    days =0; 
    editable = false;
    grid.clearGrid();
  }

  void addLiveCells()  // here, the repetitive cells should be removed
  {
    RowCol rc = grid.coordToRowCol(mouseX, mouseY);
    if (grid.isEmpty(rc)) {
      Cell c = new Cell(rc, true);
      grid.setCellAt(c, rc);
      grid.liveCells.add(c);
    }
  }
}