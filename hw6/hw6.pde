Grid grid, tempGrid;
boolean editable, getStarted;


void setup() {
  size (800, 800);
  editable = true;
  getStarted = false;
  grid = new Grid (width);
}


void draw() {
  background(0);

  if (grid != null) grid.draw();

  if (getStarted) {
    tempGrid = grid;
    //grid.liveCells = new ArrayList <Cell> ();
    //grid.nearbyCells = new ArrayList <Cell> ();
    grid = new Grid (width);
    tempGrid.count();

    for (Cell c : tempGrid.liveCells) {
      c.judgeDestiny();
      if (c.alive) {
        c.setCount(0);
        grid.setCellAt(c, c.rc);
        grid.liveCells.add(c);
      }
    }
    for (Cell nc : tempGrid.nearbyCells) {
      nc.judgeDestiny();
      if (nc.alive) {
        nc.setCount(0);
        grid.setCellAt(nc, nc.rc);
        grid.liveCells.add(nc);
      }
    }
  }
}


void mousePressed() {
  if (editable) {
    RowCol rc = grid.coordToRowCol(mouseX, mouseY);
    Cell c = new Cell (rc, true);
    grid.setCellAt(c, rc);
    grid.liveCells.add(c);
  }
  if (!editable) {
    
  }
}

void mouseDragged() {
  if (editable) {
    RowCol rc = grid.coordToRowCol(mouseX, mouseY);
    Cell c = new Cell (rc, true);
    grid.setCellAt(c, rc);
    grid.liveCells.add(c);
  }
}


void keyPressed() {

  //edit Mode or Start
  if (key == 'e' && !editable) {
    editable = true;
    getStarted = false;
    return;
  }
  if (key == 'e' && editable) {
    editable = false;
    getStarted = true;
    return;
  }

  //reset
  if (key == 'c' && editable) {
    grid = new Grid (width);
    return;
  }

  //zoom in
  if (key == '+') {
    grid.gSize += 2;
    return;
  }

  //zoom out
  if (key == '-') {
    grid.gSize -= 2; 
    return;
  }
}