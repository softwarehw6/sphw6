class RowCol {

  private int row, col;

  public RowCol( int r, int c) {
    row = r;
    col = c;
  }

  public int getRow() { 
    return row;
  }

  public int getCol() { 
    return col;
  }
}