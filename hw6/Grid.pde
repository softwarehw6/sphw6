public class Grid {

  private ArrayList <Cell> liveCells;
  private ArrayList <Cell> nearbyCells;
  private Cell [][] grid;
  private color liveCol;
  private float gSize;
  public int gridSize;

  public Grid(int size) {

    setGridSize(100);
    grid = new Cell [gridSize] [gridSize];
    liveCells = new ArrayList <Cell>();
    nearbyCells = new ArrayList <Cell>();
    liveCol = color(40, 240, 120);
    gSize = size / grid.length * 2;
  }

  public void draw() {

    //drawing background
    stroke(120);
    strokeWeight(1);
    noFill();
    for (int row = 0; row < grid.length; row ++)
      for (int col = 0; col < grid.length; col ++) {
        rect (row*gSize, col*gSize, gSize, gSize);
      }

    //drawing liveCells
    for (Cell c : liveCells) {
      fill(liveCol);
      rect(c.getCol()*gSize, c.getRow()*gSize, gSize, gSize);
    }
  }

  public void count() {

    for (Cell c : liveCells) {
      for (int i = -1; i < 2; i ++)
        for (int j = -1; j < 2; j ++) {
          RowCol rc = new RowCol (c.getRow() + i, c.getCol() + j);

          if (isEmpty(rc)) {

            Cell nc = new Cell (rc, false);
            setCellAt(nc, rc);
            nearbyCells.add(nc);
            getCell(rc).addCount();
          } else {

            if (rc.getRow() == c.getRow() && rc.getCol() == c.getCol()) getCell(rc).minusCount();
            if (getCell(rc) != null) getCell(rc).addCount();
          }
        }
    }
  }

  private RowCol coordToRowCol (int x, int y)
  {
    if (x<0 || x>width || y<0 || y>height) return null;  
    return new RowCol (int(y/gSize), int(x/gSize));
  }

  public void setGridSize(int gridSize) {
    this.gridSize = gridSize;
  }

  private void setCellAt(Cell c, RowCol rc) {
    if (!inBounds(rc)) return;
    grid [rc.getRow()] [rc.getCol()] = c;
  }

  public Cell getCell (RowCol rc) {
    if (!inBounds(rc)) return null;
    return grid [rc.getRow()] [rc.getCol()];
  }

  private boolean inBounds (RowCol rc)
  {
    if (rc==null) return false;
    if (rc.getRow() <0 || rc.getRow() >= grid.length) return false;
    if (rc.getCol() <0 || rc.getCol() >= grid.length) return false;
    return true;
  }

  public boolean isEmpty (RowCol rc)
  {
    if (!inBounds(rc)) return false;
    return grid[rc.getRow()] [rc.getCol()]==null;
  }
}  