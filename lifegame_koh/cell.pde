public class Cell {

  Grid grid;
  boolean alive;
  RowCol rc;
  int count;

  public Cell(RowCol rc, boolean alive) {
    this.rc = rc;
    this.alive = alive;
    count = 0;
  }

  public int getRow() {
    return rc.getRow();
  }

  public int getCol() {
    return rc.getCol();
  }

  protected boolean isAlive() {
    // if (THE CONDITIONS)
    return alive;
  }
  
}