class Game extends Thread
{  
  Grid grid;
  boolean editable;
  int refreshRate;
  PFont font;
  int days;

  Game(float size)
  {
    grid = new Grid (size);
    editable = true;
    refreshRate = 1000;
    font = loadFont("font.vlw");
    textFont(font, 12);
    days = 0;
  }
  
  void draw()
  {}
  
  public void run()
  {
      //background(0);
      if (grid!=null) {  
        days++;
        grid.draw();
        fill(255);
        text("Days:  " + days, 20,height-30);
      }
      try{Thread.sleep(refreshRate);}   
      catch(Exception e){println("something went wrong");}
  }
  
  //getters
  boolean isEditable()
  {return editable;}
  Grid getGrid()
  {return grid;}
  
  //setters
  void setRefresh(int r)
  {refreshRate = r;}
  void editOrPlay()
  {editable = !editable;}
  void reset(){
  days =0; 
  editable = false;
  grid.clearGrid();
  }
  
  void addLiveCells(float x, float y)
  {
    RowCol rc = grid.coordToRowCol((int)x,(int)y);
    grid.liveCells.add(new Cell(rc, true));
  }
  
}