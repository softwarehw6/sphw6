public class Grid {

  private ArrayList <Cell> liveCells;
  private ArrayList <Cell> nearbyCells;
  private Cell [][] grid;
  private color liveCol;
  private float gSize;

  public int gridSize = 50; 

  public Grid(float size) {
    grid = new Cell [gridSize] [gridSize];
    liveCells = new ArrayList <Cell>();
    nearbyCells = new ArrayList <Cell>();
    liveCol = color(0, 255, 0);
    gSize = size / grid.length;
  }

  public void draw() {

    //drawing background
    stroke(120);
    strokeWeight(1);
    noFill();
    for (int row = 0; row < grid.length; row ++)
      for (int col = 0; col < grid.length; col ++) {
        rect (row*gSize, col*gSize, gSize, gSize);
      }

    //drawing liveCells
    for (Cell c : liveCells) {
      fill(liveCol);
      rect(c.getCol()*gSize, c.getRow()*gSize, gSize, gSize);
    }
  }

  public void count() {

    for (Cell c : liveCells) {
      for (int i = -1; i < 2; i ++)
        for (int j = -1; j < 2; j ++) {
          RowCol rc = new RowCol (c.getRow() - i, c.getCol() -j);
          nearbyCells.add(new Cell (rc, false));
        }
    }
  }

  private RowCol coordToRowCol (int x, int y)
  {
    if (x<0 || x>width || y<0 || y>height) return null;  
    return new RowCol (int(y/gSize), int(x/gSize));
  }

  public Cell getCell (RowCol rc) {
    return grid [rc.getRow()][rc.getCol()];
  }
  
  public void clearGrid(){
    liveCells.clear();
    nearbyCells.clear();
    //Cell [][] grid;
    
  }
}